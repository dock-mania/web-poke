FROM node:alpine

ENV TARGET="10.0.0.10"
ENV METHOD="HEAD"

WORKDIR /web-ping
COPY app.js .

CMD ["node", "/web-ping/app.js"]
